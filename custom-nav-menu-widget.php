<?php
/*
Plugin Name: Custom navigation menu widget
Description: Add a navigation menu to your sidebar and choose the target page, post or category.
Version: 1.0
Author: Florent Chollet / David Dahan
Text Domain: custom-nav-menu-widget
Domain Path: /languages
*/


class Nav_Menu_Custom_Widget extends WP_Widget {	

	public function __construct() {
		
		load_plugin_textdomain('custom-nav-menu-widget', false, basename( dirname( __FILE__ ) ) . '/languages' );
		
		$widget_ops = array(
			'description'                 => __( 'Add a navigation menu to your sidebar and choose the target page, post or category.', 'custom-nav-menu-widget' ),
			'customize_selective_refresh' => true,
		);
		parent::__construct( 'nav_menu_custom', __( 'Custom navigation menu', 'custom-nav-menu-widget' ), $widget_ops );		
	}

	public function widget( $args, $instance ) {
		// Get menu
		$nav_menu = ! empty( $instance['nav_menu'] ) ? wp_get_nav_menu_object( $instance['nav_menu'] ) : false;

		if ( ! $nav_menu ) {
			return;
		}

		$title = ! empty( $instance['title'] ) ? $instance['title'] : '';
		$target = ! empty( $instance['target'] ) ? $instance['target'] : '';

		/** This filter is documented in wp-includes/widgets/class-wp-widget-pages.php */
		$title = apply_filters( 'widget_title', $title, $instance, $this->id_base );

		//Condition type of page
		$current = '';
		$queried_object = get_queried_object();
		if( is_page() || is_single() ):
		 	$current = $queried_object->post_type.'_'.get_the_ID();		 
		elseif( is_archive() ):
 			$current = $queried_object->taxonomy.'_'.$queried_object->term_id;
 		elseif( is_home() ):
 			$current = 'home';
		endif;
		wp_reset_query();

		if( $target && in_array( $current, $target) ): 

			echo $args['before_widget'];

			if ( $title ) {
				echo $args['before_title'] . $title . $args['after_title'];
			}

			$nav_menu_args = array(
				'fallback_cb' => '',
				'menu'        => $nav_menu,
			);

			wp_nav_menu( apply_filters( 'widget_nav_menu_args', $nav_menu_args, $nav_menu, $args, $instance ) );

			echo $args['after_widget'];

		endif;
	}
	
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		if ( ! empty( $new_instance['title'] ) ) {
			$instance['title'] = sanitize_text_field( $new_instance['title'] );
		}
		if ( ! empty( $new_instance['nav_menu'] ) ) {
			$instance['nav_menu'] = (int) $new_instance['nav_menu'];
		}
		if ( ! empty( $new_instance['target'] ) ) {
			$instance['target'] = (array) $new_instance['target'];
		}
		return $instance;
	}

	
	public function form( $instance ) {
		global $wp_customize;
		$title    = isset( $instance['title'] ) ? $instance['title'] : '';
		$nav_menu = isset( $instance['nav_menu'] ) ? $instance['nav_menu'] : '';
		$target = isset( $instance['target'] ) ? $instance['target'] : '';

		// Get menus
		$menus = wp_get_nav_menus();

		$empty_menus_style = $not_empty_menus_style = '';
		if ( empty( $menus ) ) {
			$empty_menus_style = ' style="display:none" ';
		} else {
			$not_empty_menus_style = ' style="display:none" ';
		}

		// If no menus exists, direct the user to go and create some.
		?>
		<p class="nav-menu-widget-no-menus-message" <?php echo $not_empty_menus_style; ?>>
			<?php
			if ( $wp_customize instanceof WP_Customize_Manager ) {
				$url = 'javascript: wp.customize.panel( "nav_menus" ).focus();';
			} else {
				$url = admin_url( 'nav-menus.php' );
			}
			?>
			<?php echo sprintf( __( 'No menus have been created yet. <a href="%s">Create some</a>.' ), esc_attr( $url ) ); ?>
		</p>
		<div class="nav-menu-widget-form-controls" <?php echo $empty_menus_style; ?>>
			<p>
				<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
				<input type="text" class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" value="<?php echo esc_attr( $title ); ?>"/>
			</p>
			<p>
				<label for="<?php echo $this->get_field_id( 'nav_menu' ); ?>"><?php _e( 'Select Menu:' ); ?></label>
				<select id="<?php echo $this->get_field_id( 'nav_menu' ); ?>" name="<?php echo $this->get_field_name( 'nav_menu' ); ?>">
					<option value="0"><?php _e( '&mdash; Select &mdash;' ); ?></option>
					<?php foreach ( $menus as $menu ) : ?>
						<option value="<?php echo esc_attr( $menu->term_id ); ?>" <?php selected( $nav_menu, $menu->term_id ); ?>>
							<?php echo esc_html( $menu->name ); ?>
						</option>
					<?php endforeach; ?>
				</select>
			</p>
			<p>
				<label for="<?php echo $this->get_field_id( 'target' ); ?>"><?php _e( 'Select the target:', 'custom-nav-menu-widget' ); ?></label>
				<select multiple="multiple" class="widefat" id="<?php echo $this->get_field_id( 'target' ); ?>" name="<?php echo $this->get_field_name( 'target' ); ?>[]">
					<?php 
					//On front
					if( get_option('show_on_front') == 'posts'):
						$selected = '';
						if( in_array( 'home' , $target) ) $selected= 'selected';
						echo '<option value="home" '.$selected.'>'.__('Home').'</option>';
					endif;
					//List posts
					$args = array(
						'post_type'			=> 'any',
						'posts_per_page'	=> -1,
						'orderby'			=> 'title',
						'order'				=> 'ASC'
					);
					$query = new WP_query( $args );
					if( $query->have_posts() ):
						echo '<optgroup label="'.__('Posts, pages, cpt','custom-nav-menu-widget').'">';
						while( $query->have_posts() ): $query->the_post();
							$selected = '';
							if( in_array( $query->post->post_type.'_'.get_the_ID(), $target) ) $selected= 'selected';
							echo '<option value="'.$query->post->post_type.'_'.get_the_ID().'" '.$selected.'>'.get_the_title().'</option>';
						endwhile;
						echo '</optgroup>';
					endif;
					//List categories
					$terms = get_terms('category');
					if( $terms ):
						echo '<optgroup label="'.__('Categories, taxonomies','custom-nav-menu-widget').'">';
						foreach( $terms as $term ):			
							$selected_cat = '';				
							if( in_array( $term->taxonomy.'_'.$term->term_id, $target) ) $selected_cat = 'selected';
							echo '<option value="'.$term->taxonomy.'_'.$term->term_id.'" '.$selected_cat.'>'.$term->name.'</option>';
						endforeach;
						echo '</optgroup>';
					endif;		
					wp_reset_query();			
					?>			      
				</select>
			</p>
		</div>
		<?php
	}
}
add_action('widgets_init', function(){register_widget('Nav_Menu_Custom_Widget');});